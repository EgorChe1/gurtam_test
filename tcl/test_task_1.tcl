#!/usr/bin/tclsh

proc print_data {data type} {
    if {$type == "SD"} {
        puts "date: [lindex $data 0]"
        puts "time: [lindex $data 1]"
        puts "latitude: [lindex $data 2]"
        puts "longitude: [lindex $data 3]"
        puts "speed: [lindex $data 4]"
        puts "course: [lindex $data 5]"
        puts "height: [lindex $data 6]"
        puts "sats: [lindex $data 7]"
    } elseif {$type == "M"} {
        puts "text: [lindex $data 0]"
    }
}


proc write_to_db {data type} {
    return 1
}


proc get_float_degree {data} {
    set degree [expr int($data/100)] 
    set minutes [expr ($data- $degree*100)/60]
    return [expr $degree + $minutes]
}


proc parse_data {data} {

    # remove all \r \n
    regsub -all {\\r} $data {} data
    regsub -all {\\n} $data {} data
    
    # split DATA to list
    set parse_lst [split $data "#"]
    
    if {[llength $parse_lst] == 3} {  

        if { [lindex $parse_lst 1] == "SD"} {

            # create list  
            set sd_lst [split [lindex $parse_lst 2] ";"]

            if {[llength $sd_lst] == 10} {
            
            set db_list [list [lindex $sd_lst 0]] ;# add date to list 
            lappend db_list [lindex $sd_lst 1] ;# add time to list
            
            if {[lindex $sd_lst 3] == "N"} {
                lappend db_list [get_float_degree [lindex $sd_lst 2]] ;# add latitude

            } elseif {[lindex $sd_lst 3] == "S"} {
                lappend db_list [expr [get_float_degree [lindex $sd_lst 2]]*(-1)] ;# add latitude
            } else {
                lappend db_list 0 
                puts "Invalid character in the body of the DATA field"
            }

            if {[lindex $sd_lst 5] == "E"} {
                lappend db_list [get_float_degree [lindex $sd_lst 4]] ;# add longitude

            } elseif {[lindex $sd_lst 5] == "W"} {
                lappend db_list [expr [get_float_degree [lindex $sd_lst 4]]*(-1)] ;# add longitude
            } else {
                lappend db_list 0 
                puts "Invalid character in the body of the DATA field"
            }

            lappend db_list [expr int([lindex $sd_lst 6])] ;# add speed to list
            lappend db_list [expr int([lindex $sd_lst 7])] ;# add course to list
            lappend db_list [expr int([lindex $sd_lst 8])] ;# add height to list
            lappend db_list [expr int([lindex $sd_lst 9])] ;# add sats to list

            print_data $db_list [lindex $parse_lst 1]

            } else {
                puts "bad SD packet format"
            }

        } elseif {[lindex $parse_lst 1] == "M"} {
            set db_list [list [lindex $parse_lst 2]] ;# add text to list 
            print_data $db_list [lindex $parse_lst 1]

        } else {
            puts "unknown packet format"
        }

    } else {
        puts "bad packet format"
    }
}


while 1 {
    gets stdin data
    parse_data $data
}

