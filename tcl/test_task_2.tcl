#!/usr/bin/tclsh

proc dec2bin int {
    return set res [format %04llb $int]
}

proc bin2dec bin {
    return [expr "0b$bin"]
}

proc mirror_4bit_value {value} {
    set res [string reverse [dec2bin $value]]
    return [bin2dec $res]
}

proc get_additional_params {data} {
    puts "Исходное значение $data "
    set param_1 [expr ($data >> 8) & 0xFF]
    set param_2 [expr ~($data >> 7) & 0x01]
    set mirrored [expr ($data >> 17) & 0x0F]
    set param_3 [mirror_4bit_value $mirrored]
    puts [format "Первый дополнительный параметр dec: %d hex: %llx" $param_1 $param_1]
    puts [format "Второй дополнительный параметр dec: %d hex: %llx" $param_2 $param_2]
    puts [format "Третий дополнительный параметр dec: %d hex: %llx" $param_3 $param_3]
}

while 1 {
    gets stdin data
    get_additional_params $data
}