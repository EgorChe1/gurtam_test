
def get_additional_params(data):
    '''prints additional parameters to the screen'''
    data_to_int = int(data, 16)

    print(f'Исходное значение {hex(data_to_int)}')

    param_1 = ((data_to_int >> 8) & 0xFF)
    param_2 = (~(data_to_int >> 7) & 0x01)
    param_3 = (int('{:04b}'.format((data_to_int >> 17) & 0x0F)[::-1], 2))

    print(f'Второй дополнительный параметр dec: {param_2} hex: {hex(param_2)}')
    print(f'Первый дополнительный параметр dec: {param_1} hex: {hex(param_1)}')
    print(f'Третий дополнительный параметр dec: {param_3} hex: {hex(param_3)}')


if __name__ == '__main__':
    while True:
        get_additional_params(data := input())
