import pprint as pr


PACKET_LEN = 3
SD_PACKET_LEN = 10
M_PACKET_LEN = 3
MIN_IN_DEGREES = 60
COORD_IN_DEGREES = 100
SIGN = -1

def write_to_db(packet: dict) -> None:
    '''write data packet to db'''
    pass


def print_packet(packet: dict) -> None:
    '''print data from ditonary'''
    pr.pprint(packet)


def parse_data(data: str) -> None:
    '''parses the received packet'''
    data = data.rstrip('\\r\\n')
    data_lst = data.split('#')

    if len(data_lst) == PACKET_LEN:
        if 'SD' in data_lst[1]:
            sd_dict = {}

            sd_data = data_lst[2].split(';')

            if len(sd_data) == SD_PACKET_LEN:

                sd_dict['date'] = sd_data[0]
                sd_dict['time'] = sd_data[1]

                if sd_data[3] == 'N':
                    min_in_hours = ((float(sd_data[2]) % COORD_IN_DEGREES)
                                    / MIN_IN_DEGREES)
                    sd_dict['latitude'] = (float(sd_data[2])//COORD_IN_DEGREES 
                                           + min_in_hours)

                elif sd_data[3] == 'S':
                    min_in_hours = ((float(sd_data[2]) % COORD_IN_DEGREES)
                                    / MIN_IN_DEGREES)
                    sd_dict['latitude'] = (float(sd_data[2])//COORD_IN_DEGREES
                                           + min_in_hours)*(SIGN)

                if sd_data[5] == 'E':
                    min_in_hours = ((float(sd_data[4]) % COORD_IN_DEGREES)
                                    / MIN_IN_DEGREES)
                    sd_dict['longitude'] = (float(sd_data[4])//COORD_IN_DEGREES
                                            + min_in_hours)

                elif sd_data[5] == 'W':
                    min_in_hours = ((float(sd_data[4]) % COORD_IN_DEGREES)
                                    / MIN_IN_DEGREES)
                    sd_dict['longitude'] = (float(sd_data[4])//COORD_IN_DEGREES
                                            + min_in_hours)*(SIGN)

                sd_dict['speed'] = int(sd_data[6])
                sd_dict['course'] = int(sd_data[7])
                sd_dict['height'] = int(sd_data[8])
                sd_dict['sats'] = int(sd_data[9])

                print_packet(sd_dict)
                write_to_db(sd_dict)

            else:
                print('bad SD packet format')
        elif 'M' in data_lst[1]:
            m_dict = {}

            m_dict['text'] = data_lst[2]
            print_packet(m_dict)
            write_to_db(m_dict)

        else:
            print('unknown packet format')
    else:
        print('bad packet format')


if __name__ == '__main__':
    while True:
        parse_data(data := input())
